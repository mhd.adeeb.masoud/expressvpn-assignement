package com.adeeb.expressvpn.assignment.ping

import app.cash.turbine.test
import com.adeeb.expressvpn.assignment.BaseUnitTest
import com.adeeb.expressvpn.assignment.network.model.IpLocation
import com.adeeb.expressvpn.assignment.network.ping.PingTestManagerImpl
import com.adeeb.expressvpn.assignment.network.ping.PingTester
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.kotlin.any
import org.mockito.kotlin.eq


@ExperimentalCoroutinesApi
class PingTestManagerImplTest : BaseUnitTest() {

    private lateinit var pingTestManager: PingTestManagerImpl

    @Mock
    lateinit var pingTester: PingTester

    override fun setup() {
        super.setup()
        pingTestManager = PingTestManagerImpl(pingTester, testDispatcher)
    }

    @Test
    fun testPerformOnePingOneServer() = runTest {
        pingTestManager.pingFlow.test {
            //Given
            val locationsList = listOf(IpLocation("TEST LOCATION", "TEST IP"))
            `when`(pingTester.testPing(eq("TEST IP"), any(), any())).thenReturn(10)
            //When
            pingTestManager.performPingTest(locationsList, 1)
            //Then
            awaitItem().run {
                ping == 100 && ipLocation.location == "TEST LOCATION" && ipLocation.ip == "TEST IP"
            }
            expectNoEvents()
        }
    }

    @Test
    fun testPerformOnePingOneServerTimeOut() = runTest {
        pingTestManager.pingFlow.test {
            //Given
            val locationsList = listOf(IpLocation("TEST LOCATION", "TEST IP"))
            `when`(pingTester.testPing(eq("TEST IP"), any(), any())).thenReturn(null)
            //When
            pingTestManager.performPingTest(locationsList, 1)
            //Then
            awaitItem().run {
                ping == null && ipLocation.location == "TEST LOCATION" && ipLocation.ip == "TEST IP"
            }
            expectNoEvents()
        }
    }

    @Test
    fun testPerformOnePingTenServers() = runTest {
        pingTestManager.pingFlow.test {
            //Given
            val locationsList = (1..10).map { IpLocation("TEST LOCATION$it", "TEST IP$it") }
            for (i in 1..10)
                `when`(pingTester.testPing(eq("TEST IP$i"), any(), any())).thenReturn(i * 10)
            //When
            pingTestManager.performPingTest(locationsList, 1)
            //Then
            for (i in 1..10)
                awaitItem().run {
                    ping == i * 10 && ipLocation.location == "TEST LOCATION$i" && ipLocation.ip == "TEST IP$i"
                }
            expectNoEvents()
        }
    }

    @Test
    fun testPerformXPingsTenServers() = runTest {
        pingTestManager.pingFlow.test {
            //Given
            val locationsList = (1..10).map { IpLocation("TEST LOCATION$it", "TEST IP$it") }
            for (i in 1..10)
                `when`(pingTester.testPing(eq("TEST IP$i"), any(), any())).thenReturn(i * 10)
            //When
            pingTestManager.performPingTest(locationsList, 1000)
            //Then
            for (i in 1..10)
                awaitItem().run {
                    ping == i * 10 && ipLocation.location == "TEST LOCATION$i" && ipLocation.ip == "TEST IP$i"
                }
            expectNoEvents()
        }
    }
}