package com.adeeb.expressvpn.assignment.consts

object NetworkConfig {
    const val PING_COUNT_PER_SERVER = 20
    const val PING_TIMEOUT = 2000
    const val PING_PORT = 80
    const val PING_MAX_PARALLEL_CONNECTIONS = 20
    const val API_BASE_URL = "https://private-16d939-codingchallenge2020.apiary-mock.com/"
    const val API_NETWORK_TIMEOUT = 10L
}