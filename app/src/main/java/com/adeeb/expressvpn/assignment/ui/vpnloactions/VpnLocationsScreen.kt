package com.adeeb.expressvpn.assignment.ui.vpnloactions

import BestLocationDialog
import ErrorDialogHandler
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.adeeb.expressvpn.assignment.R
import com.adeeb.expressvpn.assignment.ui.common.LocationItem


@Composable
fun VpnLocationsScreen(
    modifier: Modifier = Modifier,
    viewModel: VpnLocationsViewModel = hiltViewModel()
) {
    val scaffoldState = rememberScaffoldState()
    val uiState by viewModel.uiState.collectAsState()

    //handle error
    ErrorDialogHandler(uiState.error) { viewModel.onErrorConsumed() }

    //handle show best location dialog
    var showBestLocationDialog by rememberSaveable {
        mutableStateOf(false)
    }
    if (showBestLocationDialog) {
        BestLocationDialog(
            onDismissCallback = { showBestLocationDialog = false },
            locationDisplayModel = uiState.bestServer,
            isPingTestFinished = uiState.arePingTestsFinished
        )
    }

    Scaffold(
        modifier = modifier.fillMaxSize(), scaffoldState = scaffoldState,
        topBar = { TopAppBar(title = { Text(text = stringResource(R.string.title_main)) }) }
    ) {
        Column(
            Modifier
                .fillMaxSize()
                .animateContentSize()
        ) {
            if (uiState.locationList.isEmpty()) {
                Box(
                    modifier = modifier
                        .fillMaxWidth()
                        .weight(1f),
                    contentAlignment = Alignment.Center
                ) {
                    Text(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(horizontal = 48.dp),
                        text = stringResource(
                            R.string.text_no_locations_loaded
                        ),
                        textAlign = TextAlign.Center
                    )
                }
            } else {
                LazyColumn(modifier, contentPadding = PaddingValues(top = 16.dp)) {
                    items(uiState.locationList, { it.name }) {
                        LocationItem(Modifier.height(32.dp), it)
                    }
                }
            }
            Row(
                Modifier
                    .fillMaxWidth()
                    .padding(top = 16.dp)
                    .height(48.dp),
                horizontalArrangement = Arrangement.Center
            ) {
                if (!uiState.isLoading) {
                    Button(onClick = { viewModel.loadLocations() }) {
                        Text(
                            text = uiState.refreshButtonText
                                ?: stringResource(R.string.button_retry_default)
                        )
                    }
                } else {
                    CircularProgressIndicator(Modifier.testTag("loading"))
                }
            }
            if (uiState.locationList.isNotEmpty()) {
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.Center
                ) {
                    Button(onClick = {
                        viewModel.onBestServerPressed()
                        showBestLocationDialog = true
                    }) {
                        Text(text = stringResource(R.string.button_show_best_location))
                    }
                }
            }
            Spacer(modifier = Modifier.height(16.dp))
        }
    }
}
