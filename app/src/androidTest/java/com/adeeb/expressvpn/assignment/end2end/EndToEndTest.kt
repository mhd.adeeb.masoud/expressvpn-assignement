package com.adeeb.expressvpn.assignment.end2end

import androidx.compose.ui.test.*
import com.adeeb.expressvpn.assignment.R
import com.adeeb.expressvpn.assignment.util.BaseUiTest
import com.adeeb.expressvpn.assignment.util.waitUntilDoesNotExist
import com.adeeb.expressvpn.assignment.util.waitUntilExists
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Assert.assertEquals
import org.junit.Test

@HiltAndroidTest
class EndToEndTest : BaseUiTest() {

    @Test
    fun testEndToEndHappyScenario() {
        composeRule.run {
            waitForIdle()
            //assert title is displayed
            onNodeWithText(activity.getString(R.string.title_main)).assertIsDisplayed()

            //assert loading indicator is displayed
            onNodeWithTag("loading")
                .assertIsDisplayed()

            //wait for the loading to finish
            waitUntilDoesNotExist(hasTestTag("loading"), 10000)

            //assert list of servers exists
            onNodeWithText("Los Angeles").assertIsDisplayed()
            onNodeWithText("New York City").assertIsDisplayed()

            //assert all locations are now loading ping
            assertEquals(
                6,
                onAllNodesWithText(activity.getString(R.string.ping_text_indicator)).fetchSemanticsNodes().size
            )

            //assert buttons are showing
            onNodeWithText("Place this text on the refresh button").assertIsDisplayed()
            onNodeWithText(activity.getString(R.string.button_show_best_location)).assertIsDisplayed()

            //Press show best server button
            onNodeWithText(activity.getString(R.string.button_show_best_location)).performClick()

            //assert dialog shows that tests are still running
            waitUntilExists(hasText(activity.getString(R.string.button_ok)))
            onNodeWithText(activity.getString(R.string.title_ping_test_ongoing)).assertIsDisplayed()

            //click OK button
            onNodeWithText(activity.getString(R.string.button_ok)).performClick()

            //Wait for all ping tests to finish
            waitUntilDoesNotExist(hasText(activity.getString(R.string.ping_text_indicator)), 10000)

            //Press show best server button
            onNodeWithText(activity.getString(R.string.button_show_best_location)).performClick()

            //assert dialog shows tests are finished
            waitUntilExists(hasText(activity.getString(R.string.button_ok)))
            onNodeWithText(activity.getString(R.string.title_best_location_found)).assertIsDisplayed()

            //click OK button
            onNodeWithText(activity.getString(R.string.button_ok)).performClick()

            //click on the refresh button
            onNodeWithText("Place this text on the refresh button").performClick()

            //assert loading is shown
            waitUntilExists(hasTestTag("loading"))

            //assert old list is still shown
            onNodeWithText("Los Angeles").assertIsDisplayed()
            onNodeWithText("New York City").assertIsDisplayed()

            //assert no items are testing ping
            onNodeWithText(activity.getString(R.string.ping_text_indicator)).assertDoesNotExist()

            //wait for the loading to finish
            waitUntilDoesNotExist(hasTestTag("loading"), 10000)

            //assert all locations are now loading ping
            assertEquals(
                6,
                onAllNodesWithText(activity.getString(R.string.ping_text_indicator)).fetchSemanticsNodes().size
            )

            //Wait for all ping tests to finish
            waitUntilDoesNotExist(hasText(activity.getString(R.string.ping_text_indicator)), 10000)

        }

    }
}