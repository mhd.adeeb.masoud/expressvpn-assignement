package com.adeeb.expressvpn.assignment.di

import com.adeeb.expressvpn.assignment.network.api.GetLocationsApi
import com.adeeb.expressvpn.assignment.network.ping.PingTestManager
import com.adeeb.expressvpn.assignment.usecase.ListLocationsUseCase
import com.adeeb.expressvpn.assignment.usecase.ListLocationsUseCaseImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
object UseCaseModule {
    @Provides
    fun provideListLocationsUseCase(
        api: GetLocationsApi,
        pingTestManager: PingTestManager
    ): ListLocationsUseCase = ListLocationsUseCaseImpl(api, pingTestManager)
}