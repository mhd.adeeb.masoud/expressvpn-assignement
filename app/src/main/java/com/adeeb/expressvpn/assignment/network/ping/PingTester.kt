package com.adeeb.expressvpn.assignment.network.ping

import com.adeeb.expressvpn.assignment.consts.NetworkConfig.PING_PORT
import com.adeeb.expressvpn.assignment.consts.NetworkConfig.PING_TIMEOUT
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket

interface PingTester {
    suspend fun testPing(
        serverAddress: String,
        timeout: Int = PING_TIMEOUT,
        pingPort: Int = PING_PORT
    ): Int?
}

class SocketPingTester : PingTester {

    override suspend fun testPing(serverAddress: String, timeout: Int, pingPort: Int): Int? = try {
        val startTime = System.currentTimeMillis()
        val reachable = isReachable(serverAddress, pingPort, timeout)
        if (reachable) {
            (System.currentTimeMillis() - startTime).toInt()
        } else {
            null
        }
    } catch (_: Exception) {
        null
    }

    private fun isReachable(serverAddress: String, openPort: Int, timeOutMillis: Int): Boolean {
        try {
            Socket().use { soc ->
                soc.connect(InetSocketAddress(serverAddress, openPort), timeOutMillis)
                return true
            }
        } catch (ex: IOException) {
            return false
        }
    }

}