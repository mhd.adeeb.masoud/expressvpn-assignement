import androidx.annotation.StringRes
import androidx.compose.foundation.layout.*
import androidx.compose.material.AlertDialog
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.adeeb.expressvpn.assignment.R
import com.adeeb.expressvpn.assignment.data.LocationDisplayModel
import com.adeeb.expressvpn.assignment.ui.common.LocationItem

@Composable
fun BestLocationDialog(
    onDismissCallback: () -> Unit,
    locationDisplayModel: LocationDisplayModel?,
    isPingTestFinished: Boolean
) {
    AlertDialog(onDismissRequest = onDismissCallback,
        title = {
            Text(
                text = if (isPingTestFinished && locationDisplayModel != null)
                    stringResource(R.string.title_best_location_found)
                else if (!isPingTestFinished)
                    stringResource(R.string.title_ping_test_ongoing)
                else stringResource(R.string.title_cannot_find_best_location)
            )
        },
        text = {
            Column(Modifier.fillMaxWidth()) {
                Text(
                    text = if (locationDisplayModel != null)
                        stringResource(R.string.text_best_location_is)
                    else if (!isPingTestFinished)
                        stringResource(R.string.text_wait_for_results)
                    else stringResource(R.string.text_try_again)
                )
                if (locationDisplayModel != null) {
                    LocationItem(Modifier.height(48.dp), locationDisplayModel)
                }
            }
        },
        buttons = {
            Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.Center) {
                Button(onClick = onDismissCallback) {
                    Text(text = stringResource(R.string.button_ok))
                }
            }
            Spacer(modifier = Modifier.height(16.dp))
        })
}

@Composable
fun ErrorDialogHandler(@StringRes error: Int?, errorConsumedCallback: () -> Unit = {}) {
    var showErrorDialog by rememberSaveable { mutableStateOf(false) }
    val onDismissCallback = {
        showErrorDialog = false
        errorConsumedCallback()
    }
    showErrorDialog = error != null
    if (showErrorDialog) {
        val errorMessage = stringResource(error!!)
        AlertDialog(onDismissRequest = onDismissCallback, title = {
            Text(text = stringResource(id = R.string.title_error_dialog))
        }, text = { Text(text = errorMessage) }, buttons = {
            Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.Center) {
                Button(onClick = onDismissCallback) {
                    Text(text = stringResource(id = R.string.button_ok))
                }
            }
            Spacer(modifier = Modifier.height(16.dp))
        })
    }
}