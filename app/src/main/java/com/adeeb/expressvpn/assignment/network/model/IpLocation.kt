package com.adeeb.expressvpn.assignment.network.model

data class IpLocation(
    val location: String,
    val ip: String
)