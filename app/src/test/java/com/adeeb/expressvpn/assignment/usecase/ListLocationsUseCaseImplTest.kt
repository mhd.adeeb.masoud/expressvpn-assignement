package com.adeeb.expressvpn.assignment.usecase

import app.cash.turbine.ReceiveTurbine
import app.cash.turbine.test
import com.adeeb.expressvpn.assignment.BaseUnitTest
import com.adeeb.expressvpn.assignment.consts.NetworkConfig
import com.adeeb.expressvpn.assignment.data.LocationDisplayModel
import com.adeeb.expressvpn.assignment.data.LocationListDisplayData
import com.adeeb.expressvpn.assignment.data.PingTestResult
import com.adeeb.expressvpn.assignment.network.api.GetLocationsApi
import com.adeeb.expressvpn.assignment.network.model.*
import com.adeeb.expressvpn.assignment.network.ping.PingTestManager
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.test.runTest
import okhttp3.ResponseBody
import org.junit.Assert
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.kotlin.*
import retrofit2.HttpException
import retrofit2.Response


@ExperimentalCoroutinesApi
class ListLocationsUseCaseImplTest : BaseUnitTest() {
    companion object {
        private const val TEST_NAME = "test name"
        private const val TEST_IP = "test ip"
        private const val TEST_BUTTON_TEXT = "test button text"
    }

    @Mock
    lateinit var api: GetLocationsApi

    @Mock
    lateinit var pingTestManager: PingTestManager

    private lateinit var listLocationsUseCase: ListLocationsUseCaseImpl

    private val pingResultStream = MutableSharedFlow<ServerTestResult>()

    override fun setup() {
        super.setup()
        listLocationsUseCase = ListLocationsUseCaseImpl(api, pingTestManager)
    }

    @Test
    fun testPingResultSuccessNoPreviousPing() =
        testPingEmission {
            //api response item
            val apiResponseItem = awaitItem()

            //When
            pingResultStream.emit(ServerTestResult(IpLocation(TEST_NAME, TEST_IP), 1000))

            //Then
            val pingResultItem = awaitItem()
            Assert.assertEquals(
                apiResponseItem.refreshButtonText,
                pingResultItem.refreshButtonText
            )
            Assert.assertEquals(
                apiResponseItem.locationList[0].name,
                pingResultItem.locationList[0].name
            )

            assert(pingResultItem.locationList[0].pingResult.run {
                this is PingTestResult.Success && pingDuration == 1000
            })

            expectNoEvents()
        }

    @Test
    fun testPingResultSuccessWithPreviousPingSlowerThanResult() =
        testPingEmission {
            //api response item
            skipItems(1)
            //ping result 1
            pingResultStream.emit(ServerTestResult(IpLocation(TEST_NAME, TEST_IP), 1000))
            skipItems(1)

            //When
            pingResultStream.emit(ServerTestResult(IpLocation(TEST_NAME, TEST_IP), 900))
            val pingResultItem2 = awaitItem()

            //Then
            assert(pingResultItem2.locationList[0].pingResult.run {
                this is PingTestResult.Success && pingDuration == 900
            })
            expectNoEvents()
        }

    @Test
    fun testPingResultSuccessWithPreviousPingFasterThanResult() =
        testPingEmission {
            //api response item
            skipItems(1)
            //ping result 1
            pingResultStream.emit(ServerTestResult(IpLocation(TEST_NAME, TEST_IP), 1000))
            skipItems(1)

            //When
            pingResultStream.emit(ServerTestResult(IpLocation(TEST_NAME, TEST_IP), 2000))

            //Then
            expectNoEvents()
        }

    @Test
    fun testPingResultTimeoutWithNoPreviousPing() =
        testPingEmission {
            //api response item
            skipItems(1)

            //When
            pingResultStream.emit(ServerTestResult(IpLocation(TEST_NAME, TEST_IP), null))

            //Then
            val pingResultItem1 = awaitItem()
            assert(pingResultItem1.locationList[0].pingResult is PingTestResult.Timeout)
            expectNoEvents()
        }


    @Test
    fun testPingResultTimeoutWithPreviousPing() =
        testPingEmission {
            //api response item
            skipItems(1)
            //ping result 1
            pingResultStream.emit(ServerTestResult(IpLocation(TEST_NAME, TEST_IP), 100))
            skipItems(1)

            //When
            pingResultStream.emit(ServerTestResult(IpLocation(TEST_NAME, TEST_IP), null))

            //Then
            expectNoEvents()
        }

    @Test
    fun testLocationListHappyScenario() = runTest {
        //Given
        mockGetLocationsResponse(
            locationList = listOf(
                Location(
                    TEST_NAME, 1, 1,
                    listOf(Server(TEST_IP))
                )
            )
        )
        `when`(pingTestManager.pingFlow).thenReturn(MutableSharedFlow())
        listLocationsUseCase.subscribeForUpdates(testScope).test {

            //When
            listLocationsUseCase.loadLocationsList()

            //Then
            verify(pingTestManager).performPingTest(
                argThat { size == 1 && get(0).run { location == TEST_NAME && ip == TEST_IP } },
                eq(NetworkConfig.PING_COUNT_PER_SERVER)
            )

            assert(awaitItem().run {
                locationList.size == 1 &&
                        locationList[0] == LocationDisplayModel(TEST_NAME) &&
                        refreshButtonText == TEST_BUTTON_TEXT
            })

            expectNoEvents()
        }
    }


    @Test(expected = HttpException::class)
    fun testLocationListExceptionScenario() = runTest {
        //Given
        `when`(pingTestManager.pingFlow).thenReturn(MutableSharedFlow())
        `when`(api.getLocations()).thenThrow(
            HttpException(
                Response.error<String>(
                    401,
                    ResponseBody.create(null, "")
                )
            )
        )
        //When
        listLocationsUseCase.loadLocationsList()
        //Then throw exception ^
    }

    @Test
    fun testLocationListEmptyListScenario() = runTest {
        //Given
        `when`(pingTestManager.pingFlow).thenReturn(MutableSharedFlow())
        mockGetLocationsResponse()
        listLocationsUseCase.subscribeForUpdates(testScope).test {

            //When
            listLocationsUseCase.loadLocationsList()

            //Then
            verify(pingTestManager, never()).performPingTest(any(), any())

            assert(awaitItem().run {
                locationList.isEmpty() && refreshButtonText == TEST_BUTTON_TEXT
            })
        }

    }

    private suspend fun mockGetLocationsResponse(locationList: List<Location> = listOf()) {
        `when`(api.getLocations()).thenReturn(
            LocationsResponse(
                listOf(), locationList, TEST_BUTTON_TEXT
            )
        )
    }

    private fun testPingEmission(
        tests: suspend ReceiveTurbine<LocationListDisplayData>.() -> Unit
    ) = runTest {
        mockGetLocationsResponse(
            locationList = listOf(
                Location(
                    TEST_NAME, 1, 1,
                    listOf(Server(TEST_IP))
                )
            )
        )
        `when`(pingTestManager.pingFlow).thenReturn(pingResultStream)
        listLocationsUseCase.subscribeForUpdates(testScope).test {
            listLocationsUseCase.loadLocationsList()
            tests(this)
        }
    }

}