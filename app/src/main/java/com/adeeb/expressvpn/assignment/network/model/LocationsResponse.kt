package com.adeeb.expressvpn.assignment.network.model

import com.tickaroo.tikxml.annotation.Element
import com.tickaroo.tikxml.annotation.Path
import com.tickaroo.tikxml.annotation.PropertyElement
import com.tickaroo.tikxml.annotation.Xml

@Xml(name = "expressvpn")
data class LocationsResponse(
    @Path("icons")
    @Element
    val icons: List<Icon>,
    @Path("locations")
    @Element
    val locations: List<Location>,
    @PropertyElement(name = "button_text")
    val refreshButtonText: String
)
