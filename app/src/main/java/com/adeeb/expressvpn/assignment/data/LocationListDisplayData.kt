package com.adeeb.expressvpn.assignment.data

data class LocationListDisplayData(
    val locationList: List<LocationDisplayModel> = listOf(),
    val refreshButtonText: String? = null
)
