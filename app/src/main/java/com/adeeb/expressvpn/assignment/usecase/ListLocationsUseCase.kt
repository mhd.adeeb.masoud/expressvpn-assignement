package com.adeeb.expressvpn.assignment.usecase

import androidx.annotation.WorkerThread
import com.adeeb.expressvpn.assignment.data.LocationDisplayModel
import com.adeeb.expressvpn.assignment.data.LocationListDisplayData
import com.adeeb.expressvpn.assignment.data.PingTestResult
import com.adeeb.expressvpn.assignment.network.api.GetLocationsApi
import com.adeeb.expressvpn.assignment.network.model.IpLocation
import com.adeeb.expressvpn.assignment.network.ping.PingTestManager
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.*


interface ListLocationsUseCase {
    suspend fun loadLocationsList()
    fun subscribeForUpdates(scope: CoroutineScope): SharedFlow<LocationListDisplayData>
}

class ListLocationsUseCaseImpl(
    private val api: GetLocationsApi,
    private val pingTestManager: PingTestManager
) : ListLocationsUseCase {

    private val publisher = MutableSharedFlow<LocationListDisplayData>(0)

    private var locations = listOf<LocationDisplayModel>()
    private var refreshButtonText: String? = null

    override fun subscribeForUpdates(scope: CoroutineScope): SharedFlow<LocationListDisplayData> {
        pingTestManager.pingFlow.onEach { serverResult ->
            val locationModel =
                locations.find { it.name == serverResult.ipLocation.location } ?: return@onEach
            val oldPingResult = locationModel.pingResult

            val newPingResult = when (oldPingResult) {
                is PingTestResult.Success -> {
                    if (serverResult.ping == null || serverResult.ping >= oldPingResult.pingDuration) return@onEach
                    PingTestResult.Success(serverResult.ping)
                }
                else -> serverResult.ping?.let { PingTestResult.Success(it) }
                    ?: PingTestResult.Timeout
            }

            if (newPingResult == oldPingResult) return@onEach

            locations = locations.map {
                if (it.name == locationModel.name) locationModel.copy(
                    pingResult = newPingResult
                ) else it
            }

            if (!locations.any { it.name == locationModel.name && it.pingResult is PingTestResult.Unknown })
                publisher.emit(
                    LocationListDisplayData(
                        locations,
                        refreshButtonText
                    )
                )
        }.launchIn(scope)
        return publisher.asSharedFlow()
    }

    @WorkerThread
    override suspend fun loadLocationsList() {
        //load API
        val response = api.getLocations()
        locations = response.locations.map { location ->
            LocationDisplayModel(
                location.name,
                response.icons.firstOrNull { it.id == location.iconId }?.bitmap
            )
        }
        refreshButtonText = response.refreshButtonText

        publisher.emit(LocationListDisplayData(locations, response.refreshButtonText))

        if (locations.isEmpty()) return
        //Perform ping tests
        pingTestManager.performPingTest(response.locations.flatMap { location ->
            location.serverList.map {
                IpLocation(
                    location.name,
                    it.ip
                )
            }
        })

    }

}