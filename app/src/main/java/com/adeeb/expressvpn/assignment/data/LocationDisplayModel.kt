package com.adeeb.expressvpn.assignment.data

import android.graphics.Bitmap

data class LocationDisplayModel(
    val name: String,
    val icon: Bitmap? = null,
    val pingResult: PingTestResult = PingTestResult.Unknown
)
