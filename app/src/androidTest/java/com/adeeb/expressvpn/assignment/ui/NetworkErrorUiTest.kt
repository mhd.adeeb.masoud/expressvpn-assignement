package com.adeeb.expressvpn.assignment.ui

import androidx.annotation.StringRes
import androidx.compose.ui.test.*
import com.adeeb.expressvpn.assignment.R
import com.adeeb.expressvpn.assignment.data.LocationDisplayModel
import com.adeeb.expressvpn.assignment.data.LocationListDisplayData
import com.adeeb.expressvpn.assignment.data.PingTestResult
import com.adeeb.expressvpn.assignment.di.UseCaseModule
import com.adeeb.expressvpn.assignment.usecase.ListLocationsUseCase
import com.adeeb.expressvpn.assignment.util.BaseUiTest
import com.adeeb.expressvpn.assignment.util.waitUntilDoesNotExist
import com.adeeb.expressvpn.assignment.util.waitUntilExists
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.withContext
import okhttp3.ResponseBody
import org.junit.Assert.assertEquals
import org.junit.Test
import retrofit2.HttpException
import retrofit2.Response
import java.net.SocketTimeoutException

private const val TEST_BUTTON = "Test Button"

@UninstallModules(UseCaseModule::class)
@HiltAndroidTest
class NetworkErrorUiTest : BaseUiTest() {

    @Module
    @InstallIn(SingletonComponent::class)
    object FakeUseCaseModule {
        @Provides
        fun provideUseCase(): ListLocationsUseCase = object : ListLocationsUseCase {
            var callNumber = 1
            val publisher = MutableSharedFlow<LocationListDisplayData>()

            override suspend fun loadLocationsList() {
                when (callNumber++) {
                    1 -> {
                        withContext(Dispatchers.IO) {
                            Thread.sleep(2000)
                        }
                        throw SocketTimeoutException()
                    }
                    2 -> {
                        withContext(Dispatchers.IO) {
                            Thread.sleep(500)
                        }
                        throw HttpException(Response.error<String>(403, ResponseBody.create(null, "")))
                    }
                    3 -> {
                        withContext(Dispatchers.IO) {
                            Thread.sleep(500)
                        }
                        throw HttpException(Response.error<String>(500, ResponseBody.create(null, "")))
                    }
                    else -> {
                        withContext(Dispatchers.IO) {
                            Thread.sleep(500)
                        }
                        //load 10 servers
                        publisher.emit(
                            LocationListDisplayData(
                                (1..10).map { LocationDisplayModel("Server $it") },
                                TEST_BUTTON
                            )
                        )

                        withContext(Dispatchers.IO) {
                            Thread.sleep(500)
                        }
                        //time out all servers
                        publisher.emit(LocationListDisplayData((1..10).map {
                            LocationDisplayModel(
                                "Server $it",
                                pingResult = PingTestResult.Timeout
                            )
                        }, TEST_BUTTON))
                    }
                }
            }

            override fun subscribeForUpdates(scope: CoroutineScope): SharedFlow<LocationListDisplayData> {
                return publisher
            }
        }
    }

    @Test
    fun testLoadingErrorStates() {
        composeRule.run {
            waitForIdle()
            //assert title is displayed
            onNodeWithText(activity.getString(R.string.title_main)).assertIsDisplayed()


            //--------------------------
            //Test timeout error

            testErrorDisplayed(R.string.error_timeout)

            //--------------------------
            //Test 403 error

            //press the retry button
            onNodeWithText(activity.getString(R.string.button_retry_default)).performClick()

            testErrorDisplayed(R.string.error_403)

            //--------------------------
            //Test Generic error

            //press the retry button
            onNodeWithText(activity.getString(R.string.button_retry_default)).performClick()

            testErrorDisplayed(R.string.error_unknown)

            //--------------------------
            //Test success response

            //press the retry button
            onNodeWithText(activity.getString(R.string.button_retry_default)).performClick()

            //assert loading indicator is displayed
            onNodeWithTag("loading").assertIsDisplayed()

            //wait for the loading to finish
            waitUntilDoesNotExist(hasTestTag("loading"))

            //assert no error
            onNodeWithText(activity.getString(R.string.title_error_dialog)).assertDoesNotExist()

            //assert retry button changed
            onNodeWithText(TEST_BUTTON).assertExists()

            //assert servers are displayed
            for (i in 1..10) onNodeWithText("Server $i").assertIsDisplayed()

            //assert ping tests are ongoing
            assertEquals(
                10,
                onAllNodesWithText(activity.getString(R.string.ping_text_indicator)).fetchSemanticsNodes().size
            )

            //--------------------------
            //Test clicking show best location before ping tests are finished

            //Press show best server button
            onNodeWithText(activity.getString(R.string.button_show_best_location)).performClick()

            //assert dialog shows that tests are still running
            waitUntilExists(hasText(activity.getString(R.string.button_ok)))
            onNodeWithText(activity.getString(R.string.title_ping_test_ongoing)).assertIsDisplayed()
            onNodeWithText(activity.getString(R.string.text_wait_for_results)).assertIsDisplayed()

            //close the dialog
            onNodeWithText(activity.getString(R.string.button_ok)).performClick()

            //--------------------------
            //Test ping tests time out

            //wait for the ping tests to finish
            waitUntilDoesNotExist(hasText(activity.getString(R.string.ping_text_indicator)))

            //assert all ping tests time out
            assertEquals(
                10,
                onAllNodesWithText(activity.getString(R.string.text_timeout)).fetchSemanticsNodes().size
            )

            //--------------------------
            //Test clicking show best location after ping tests time out

            //Press show best server button
            onNodeWithText(activity.getString(R.string.button_show_best_location)).performClick()

            //assert dialog shows that tests finished with a negative result
            waitUntilExists(hasText(activity.getString(R.string.button_ok)))
            onNodeWithText(activity.getString(R.string.title_cannot_find_best_location)).assertIsDisplayed()
            onNodeWithText(activity.getString(R.string.text_try_again)).assertIsDisplayed()

        }
    }

    private fun testErrorDisplayed( @StringRes error: Int) = composeRule.run {
        //assert loading indicator is displayed
        onNodeWithTag("loading").assertIsDisplayed()

        //wait for the loading to finish
        waitUntilDoesNotExist(hasTestTag("loading"), 3000)


        //assert error is displayed
        waitUntilExists(hasText(activity.getString(error)))
        onNodeWithText(activity.getString(R.string.title_error_dialog)).assertIsDisplayed()
        onNodeWithText(activity.getString(R.string.button_ok)).assertIsDisplayed()

        //Tap ok button
        onNodeWithText(activity.getString(R.string.button_ok)).performClick()

        //assert empty list text is displayed
        onNodeWithText(activity.getString(R.string.text_no_locations_loaded)).assertExists()


        //assert retry button is displayed
        onNodeWithText(activity.getString(R.string.button_retry_default)).assertExists()
    }

}