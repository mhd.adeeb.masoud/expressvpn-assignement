package com.adeeb.expressvpn.assignment.di

import com.adeeb.expressvpn.assignment.consts.NetworkConfig.API_BASE_URL
import com.adeeb.expressvpn.assignment.consts.NetworkConfig.API_NETWORK_TIMEOUT
import com.adeeb.expressvpn.assignment.network.api.GetLocationsApi
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.tickaroo.tikxml.TikXml
import com.tickaroo.tikxml.retrofit.TikXmlConverterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit

@Module
@InstallIn(ViewModelComponent::class)
object NetworkModule {

    @Provides
    fun provideOkHttpClient(): OkHttpClient = OkHttpClient.Builder()
        .connectTimeout(API_NETWORK_TIMEOUT, TimeUnit.SECONDS)
        .callTimeout(API_NETWORK_TIMEOUT, TimeUnit.SECONDS)
        .readTimeout(API_NETWORK_TIMEOUT, TimeUnit.SECONDS)
        .writeTimeout(API_NETWORK_TIMEOUT, TimeUnit.SECONDS)
        .build()

    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
        .client(okHttpClient)
        .baseUrl(API_BASE_URL)
        .addConverterFactory(
            TikXmlConverterFactory.create(
                TikXml.Builder().exceptionOnUnreadXml(false).build()
            )
        )
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()

    @Provides
    fun providesGetLocationsApi(retrofit: Retrofit): GetLocationsApi =
        retrofit.create(GetLocationsApi::class.java)
}