package com.adeeb.expressvpn.assignment.ui.vpnloactions

import androidx.annotation.StringRes
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.adeeb.expressvpn.assignment.R
import com.adeeb.expressvpn.assignment.data.LocationDisplayModel
import com.adeeb.expressvpn.assignment.data.PingTestResult
import com.adeeb.expressvpn.assignment.usecase.ListLocationsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import javax.inject.Inject

@HiltViewModel
class VpnLocationsViewModel @Inject constructor(
    private val listLocationsUseCase: ListLocationsUseCase
) : ViewModel() {

    private val _uiState = MutableStateFlow(VpnLocationsUiState())
    val uiState = _uiState.asStateFlow()

    private var job: Job? = null

    init {
        loadLocations()

        listLocationsUseCase.subscribeForUpdates(viewModelScope).onEach {
            _uiState.value = _uiState.value.copy(
                isLoading = false, error = null,
                locationList = it.locationList,
                refreshButtonText = it.refreshButtonText
            )
        }.launchIn(viewModelScope)
    }

    fun loadLocations() {
        job?.let { if (it.isActive) it.cancel(CancellationException("Refresh is requested")) }
        _uiState.value = _uiState.value.copy(
            isLoading = true,
            error = null,
            bestServer = null,
            arePingTestsFinished = false
        )
        job = viewModelScope.launch {
            try {
                listLocationsUseCase.loadLocationsList()
            } catch (ex: Exception) {
                if (ex is CancellationException) return@launch

                val error = if (ex is HttpException && ex.code() == 403) R.string.error_403
                else if (ex is UnknownHostException || ex is SocketTimeoutException) R.string.error_timeout
                else R.string.error_unknown
                _uiState.value = _uiState.value.copy(error = error, isLoading = false)
            }
        }
    }

    fun onErrorConsumed() {
        _uiState.value = _uiState.value.copy(error = null)
    }

    fun onBestServerPressed() {
        val locationList = uiState.value.locationList
        val bestServer = locationList.minByOrNull {
            if (it.pingResult is PingTestResult.Success) it.pingResult.pingDuration else Int.MAX_VALUE
        }

        _uiState.value = _uiState.value.copy(
            bestServer = if (bestServer != null && bestServer.pingResult is PingTestResult.Success) bestServer else null,
            arePingTestsFinished = !uiState.value.locationList.any { it.pingResult is PingTestResult.Unknown })
    }
}

data class VpnLocationsUiState(
    val isLoading: Boolean = false,
    val arePingTestsFinished: Boolean = false,
    @StringRes val error: Int? = null,
    val locationList: List<LocationDisplayModel> = listOf(),
    val refreshButtonText: String? = null,
    val bestServer: LocationDisplayModel? = null
)
