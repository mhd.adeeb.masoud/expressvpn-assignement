package com.adeeb.expressvpn.assignment.network.ping

import com.adeeb.expressvpn.assignment.consts.NetworkConfig.PING_COUNT_PER_SERVER
import com.adeeb.expressvpn.assignment.network.model.IpLocation
import com.adeeb.expressvpn.assignment.network.model.ServerTestResult
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Job
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch

interface PingTestManager {
    val pingFlow: Flow<ServerTestResult>
    suspend fun performPingTest(
        serverLocationList: List<IpLocation>,
        numberOfPingsPerServer: Int = PING_COUNT_PER_SERVER
    )
}

class PingTestManagerImpl(
    private val pingTester: PingTester,
    private val dispatcher: CoroutineDispatcher
) : PingTestManager {

    private val _pingFlow = MutableSharedFlow<ServerTestResult>(0)
    override val pingFlow = _pingFlow.asSharedFlow()
    private var prevJobs = mutableListOf<Job>()

    override suspend fun performPingTest(
        serverLocationList: List<IpLocation>,
        numberOfPingsPerServer: Int
    ) {
        prevJobs.forEach { if (it.isActive) it.cancel() }
        prevJobs.clear()
        serverLocationList.forEach { locationIp ->
            prevJobs.add(MainScope().launch(dispatcher) {
                var sum = 0
                var numberOfPings = 0
                repeat(numberOfPingsPerServer) {
                    val pingResult = pingTester.testPing(locationIp.ip)
                    if (pingResult != null) {
                        sum += pingResult
                        numberOfPings++
                    }
                }

                _pingFlow.emit(
                    ServerTestResult(
                        locationIp,
                        if (numberOfPings == 0) null else sum / numberOfPings
                    )
                )
            })
        }
    }
}