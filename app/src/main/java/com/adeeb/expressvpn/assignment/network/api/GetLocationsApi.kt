package com.adeeb.expressvpn.assignment.network.api

import com.adeeb.expressvpn.assignment.network.model.LocationsResponse
import retrofit2.http.GET

interface GetLocationsApi {
    @GET("/locations")
    suspend fun getLocations(): LocationsResponse
}