package com.adeeb.expressvpn.assignment.network.model

data class ServerTestResult(
    val ipLocation: IpLocation,
    val ping: Int?
)
