package com.adeeb.expressvpn.assignment.ui.theme

import androidx.compose.ui.graphics.Color

val Red200 = Color(0xFFBF4242)
val Red500 = Color(0xFFC94746)
val Red700 = Color(0xFFAF3331)
val Teal200 = Color(0xFF03DAC5)