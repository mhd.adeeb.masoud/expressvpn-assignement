package com.adeeb.expressvpn.assignment.network.model

import com.tickaroo.tikxml.annotation.Attribute
import com.tickaroo.tikxml.annotation.Element
import com.tickaroo.tikxml.annotation.Xml

@Xml(name = "location")
data class Location(
    @Attribute(name = "name")
    val name: String,
    @Attribute(name = "sort_order")
    val sortOrder: Int,
    @Attribute(name = "icon_id")
    val iconId: Int,
    @Element
    val serverList: List<Server>
)