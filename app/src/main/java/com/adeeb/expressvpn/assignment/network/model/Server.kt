package com.adeeb.expressvpn.assignment.network.model

import com.tickaroo.tikxml.annotation.Attribute
import com.tickaroo.tikxml.annotation.Xml

@Xml(name = "server")
data class Server(
    @Attribute(name = "ip")
    val ip: String
)
