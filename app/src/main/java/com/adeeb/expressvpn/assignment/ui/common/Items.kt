package com.adeeb.expressvpn.assignment.ui.common

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.QuestionMark
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.adeeb.expressvpn.assignment.R
import com.adeeb.expressvpn.assignment.data.LocationDisplayModel
import com.adeeb.expressvpn.assignment.data.PingTestResult


@Composable
fun LocationItem(
    modifier: Modifier = Modifier,
    model: LocationDisplayModel
) {
    Row(
        horizontalArrangement = Arrangement.spacedBy(8.dp),
        modifier = modifier
            .fillMaxWidth()
            .padding(top = 8.dp, start = 8.dp)
    ) {

        Column(
            Modifier
                .padding(start = 4.dp)
                .fillMaxHeight(),
            verticalArrangement = Arrangement.Center
        ) {
            if (model.icon != null) {
                Image(
                    bitmap = model.icon.asImageBitmap(),
                    contentScale = ContentScale.FillWidth,
                    contentDescription = stringResource(R.string.cd_flag),
                    modifier = Modifier.size(32.dp)
                )
            } else {
                Icon(
                    imageVector = Icons.Filled.QuestionMark,
                    contentDescription = stringResource(R.string.cd_unknown_flag),
                    modifier = Modifier.size(32.dp)
                )
            }
        }
        Column(
            Modifier
                .padding(start = 4.dp)
                .fillMaxHeight()
                .weight(1f),
            verticalArrangement = Arrangement.Center
        ) {
            Text(
                text = model.name,
                textAlign = TextAlign.Center
            )
        }
        Column(
            Modifier
                .padding(horizontal = 8.dp)
                .fillMaxHeight()
                .alpha(.4f),
            verticalArrangement = Arrangement.Center
        ) {
            Text(
                text = when (model.pingResult) {
                    is PingTestResult.Success -> stringResource(
                        R.string.text_ms,
                        model.pingResult.pingDuration
                    )
                    PingTestResult.Timeout -> stringResource(R.string.text_timeout)
                    PingTestResult.Unknown -> stringResource(R.string.ping_text_indicator)
                },
                textAlign = TextAlign.Center
            )
        }
    }
}