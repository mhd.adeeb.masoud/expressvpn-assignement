package com.adeeb.expressvpn.assignment.ui.vpnloactions

import app.cash.turbine.test
import com.adeeb.expressvpn.assignment.BaseUnitTest
import com.adeeb.expressvpn.assignment.R
import com.adeeb.expressvpn.assignment.data.LocationDisplayModel
import com.adeeb.expressvpn.assignment.data.LocationListDisplayData
import com.adeeb.expressvpn.assignment.data.PingTestResult
import com.adeeb.expressvpn.assignment.usecase.ListLocationsUseCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.test.runTest
import okhttp3.ResponseBody
import org.junit.Assert.assertEquals
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.kotlin.any
import org.mockito.kotlin.verify
import retrofit2.HttpException
import retrofit2.Response
import java.net.SocketTimeoutException

@ExperimentalCoroutinesApi
class VpnLocationsViewModelTest : BaseUnitTest() {

    @Mock
    lateinit var listLocationsUseCase: ListLocationsUseCase

    private lateinit var viewModel: VpnLocationsViewModel

    private val displayDataPublisher = MutableSharedFlow<LocationListDisplayData>()

    override fun setup() {
        super.setup()
        `when`(listLocationsUseCase.subscribeForUpdates(any())).thenReturn(displayDataPublisher)
    }

    @Test
    fun testInitialUiState() {
        runTest {
            initiateTest()
            viewModel.uiState.test {
                assertEquals(VpnLocationsUiState(isLoading = true), awaitItem())
                expectNoEvents()
            }
        }
    }

    @Test
    fun testLoadLocationsFailFirstTimeUnknownError() {
        runTest {
            `when`(listLocationsUseCase.loadLocationsList()).thenAnswer { throw Exception("test") }
            initiateTest()
            viewModel.uiState.test {
                assertEquals(
                    VpnLocationsUiState(isLoading = false, error = R.string.error_unknown),
                    awaitItem()
                )
                expectNoEvents()
            }
        }
    }

    @Test
    fun testLoadLocationsFailFirstTime403() {
        runTest {
            `when`(listLocationsUseCase.loadLocationsList()).thenAnswer {
                throw HttpException(
                    Response.error<String>(403, ResponseBody.create(null, "test"))
                )
            }
            initiateTest()
            viewModel.uiState.test {
                assertEquals(
                    VpnLocationsUiState(isLoading = false, error = R.string.error_403),
                    awaitItem()
                )
                expectNoEvents()
            }
        }
    }

    @Test
    fun testLoadLocationsFailSecondTime() {
        runTest {
            //Given
            initiateTest()
            viewModel.uiState.test {
                //loading first time
                assertEquals(VpnLocationsUiState(isLoading = true), awaitItem())
                //success first time
                displayDataPublisher.emit(LocationListDisplayData(listOf(), "refresh"))
                assertEquals(
                    VpnLocationsUiState(
                        isLoading = false,
                        locationList = listOf(),
                        refreshButtonText = "refresh"
                    ), awaitItem()
                )
                //Given
                `when`(listLocationsUseCase.loadLocationsList()).thenAnswer {
                    throw SocketTimeoutException(
                        "test"
                    )
                }

                //Load locations second time
                viewModel.loadLocations()

                //loading state
                assertEquals(true, awaitItem().isLoading)

                //display error with previous data
                assertEquals(
                    VpnLocationsUiState(
                        isLoading = false, locationList = listOf(),
                        refreshButtonText = "refresh", error = R.string.error_timeout
                    ),
                    awaitItem()
                )

                //consume the error
                viewModel.onErrorConsumed()

                //remove the error from the UI
                assertEquals(
                    VpnLocationsUiState(
                        isLoading = false, locationList = listOf(),
                        refreshButtonText = "refresh", error = null
                    ),
                    awaitItem()
                )
                expectNoEvents()
            }
        }
    }

    @Test
    fun testLoadLocationsSuccessFirstTimeEmptyList() {
        runTest {
            initiateTest()
            viewModel.uiState.test {
                //loading state
                assertEquals(VpnLocationsUiState(isLoading = true), awaitItem())
                //success result
                displayDataPublisher.emit(LocationListDisplayData(listOf(), "refresh"))
                assertEquals(
                    VpnLocationsUiState(
                        isLoading = false,
                        locationList = listOf(),
                        refreshButtonText = "refresh"
                    ), awaitItem()
                )
                expectNoEvents()
            }
        }
    }

    @Test
    fun testLoadLocationsSuccessFirstTimeNonEmptyList() {
        runTest {
            initiateTest()
            viewModel.uiState.test {
                //loading state
                assertEquals(VpnLocationsUiState(isLoading = true), awaitItem())
                //success result
                displayDataPublisher.emit(
                    LocationListDisplayData(
                        listOf(LocationDisplayModel("a1")),
                        "refresh"
                    )
                )
                assertEquals(
                    VpnLocationsUiState(
                        isLoading = false,
                        locationList = listOf(LocationDisplayModel("a1")),
                        refreshButtonText = "refresh"
                    ), awaitItem()
                )
                expectNoEvents()
            }
        }
    }

    @Test
    fun testReloadLocationsSuccessNonEmptyList() {
        runTest {
            initiateTest()
            viewModel.uiState.test {
                //loading state
                assertEquals(VpnLocationsUiState(isLoading = true), awaitItem())
                //first success response
                displayDataPublisher.emit(
                    LocationListDisplayData(
                        listOf(LocationDisplayModel("a1")),
                        "refresh"
                    )
                )
                assertEquals(
                    VpnLocationsUiState(
                        isLoading = false,
                        locationList = listOf(LocationDisplayModel("a1")),
                        refreshButtonText = "refresh"
                    ), awaitItem()
                )
                viewModel.loadLocations()
                //reset to loading state with previous result still in display
                assertEquals(
                    VpnLocationsUiState(
                        isLoading = true,
                        locationList = listOf(LocationDisplayModel("a1")),
                        refreshButtonText = "refresh"
                    ), awaitItem()
                )
                //second success response
                displayDataPublisher.emit(
                    LocationListDisplayData(
                        listOf(LocationDisplayModel("a2")),
                        "refresh!!"
                    )
                )
                //update with new info
                assertEquals(
                    VpnLocationsUiState(
                        isLoading = false,
                        locationList = listOf(LocationDisplayModel("a2")),
                        refreshButtonText = "refresh!!"
                    ), awaitItem()
                )
                expectNoEvents()
            }
        }
    }

    @Test
    fun testUpdatePingStatus() {
        runTest {
            initiateTest()
            viewModel.uiState.test {
                //loading state
                assertEquals(VpnLocationsUiState(isLoading = true), awaitItem())
                //first success response
                displayDataPublisher.emit(
                    LocationListDisplayData(
                        listOf(LocationDisplayModel("a1")),
                        "refresh"
                    )
                )
                assertEquals(
                    VpnLocationsUiState(
                        isLoading = false,
                        locationList = listOf(LocationDisplayModel("a1")),
                        refreshButtonText = "refresh"
                    ), awaitItem()
                )

                //update ping
                displayDataPublisher.emit(
                    LocationListDisplayData(
                        listOf(
                            LocationDisplayModel(
                                name = "a1",
                                pingResult = PingTestResult.Success(100)
                            )
                        ),
                        "refresh"
                    )
                )
                //update with new info
                assertEquals(
                    VpnLocationsUiState(
                        isLoading = false,
                        locationList = listOf(
                            LocationDisplayModel(
                                name = "a1",
                                pingResult = PingTestResult.Success(100)
                            )
                        ),
                        refreshButtonText = "refresh"
                    ), awaitItem()
                )
                expectNoEvents()
            }
        }
    }


    private suspend fun initiateTest() {
        viewModel = VpnLocationsViewModel(listLocationsUseCase)
        verify(listLocationsUseCase).loadLocationsList()
        verify(listLocationsUseCase).subscribeForUpdates(any())
    }

}