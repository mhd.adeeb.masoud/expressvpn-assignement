package com.adeeb.expressvpn.assignment.network.model

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import com.tickaroo.tikxml.annotation.Attribute
import com.tickaroo.tikxml.annotation.TextContent
import com.tickaroo.tikxml.annotation.Xml

@Xml(name = "icon")
data class Icon(
    @Attribute(name = "id")
    val id: Int,
    @TextContent
    val base64: String
) {
    private var decodedBitmap: Bitmap? = null
    val bitmap: Bitmap?
        get() {
            if (decodedBitmap == null) {
                val decodedString: ByteArray = Base64.decode(base64, Base64.DEFAULT)
                decodedBitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
            }
            return decodedBitmap
        }
}