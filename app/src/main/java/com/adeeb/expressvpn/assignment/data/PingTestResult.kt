package com.adeeb.expressvpn.assignment.data

sealed interface PingTestResult {
    object Unknown : PingTestResult
    object Timeout : PingTestResult
    data class Success(val pingDuration: Int) : PingTestResult
}