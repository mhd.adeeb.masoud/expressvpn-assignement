package com.adeeb.expressvpn.assignment.di

import com.adeeb.expressvpn.assignment.consts.NetworkConfig.PING_MAX_PARALLEL_CONNECTIONS
import com.adeeb.expressvpn.assignment.network.ping.PingTestManager
import com.adeeb.expressvpn.assignment.network.ping.PingTestManagerImpl
import com.adeeb.expressvpn.assignment.network.ping.PingTester
import com.adeeb.expressvpn.assignment.network.ping.SocketPingTester
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi

@Module
@InstallIn(SingletonComponent::class)
@ExperimentalCoroutinesApi
object PingTestModule {

    @Provides
    fun providePingTestManager(pingTester: PingTester): PingTestManager = PingTestManagerImpl(
        pingTester,
        Dispatchers.IO.limitedParallelism(PING_MAX_PARALLEL_CONNECTIONS)
    )

    @Provides
    fun providePingTester(): PingTester = SocketPingTester()
}