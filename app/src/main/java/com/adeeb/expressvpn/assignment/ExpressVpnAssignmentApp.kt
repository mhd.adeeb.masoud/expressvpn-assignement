package com.adeeb.expressvpn.assignment

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ExpressVpnAssignmentApp : Application() {
}