# ExpressVpn Android Assignment
This is an implementation of ExpressVpn Assignment on the Android platform.

## Tech Stack
- Kotlin programming language
- CLEAN project architecture
- Jetpack Compose & Material Design for the UI layer
- MVVM & StateFlow for the presentation layer
- Kotlin Coroutines for asynchronous code handling
- Hilt for dependency injection
- Retrofit & OkHttp for networking
- TikXML for XML parsing
- JUnit & Mockito & Turbine for unit testing.
- Compose & Hilt for UI testing

## Features
- get locations list.
- measure ping in parallel for servers.
- handle errors and edge cases.
- Supports Material Design theming.
- Supports landscape and portrait orientations.
- Supports dark and light modes.
- Unit tests for major classes.
- End to end tesing for the happy scenario.
- UI testing for the network errors cases.

## Building, Testing, and configuration
- run the app using `./gradlew assembleDebug && ./gradlew installDebug`
- run the unit tests using `./gradlew testDebug`
- run the ui tests using `./gradlew connectedAndroidTest`
- you can configure the network constants from `consts/NetworkConfig`
